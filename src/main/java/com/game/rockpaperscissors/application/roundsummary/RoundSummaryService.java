package com.game.rockpaperscissors.application.roundsummary;

import java.util.ArrayList;


import org.springframework.stereotype.Service;
import com.game.rockpaperscissors.domain.round.Round;
import com.game.rockpaperscissors.domain.round.RoundRepository;

@Service
public class RoundSummaryService {
	public RoundRepository roundRepository;
	
	public RoundSummaryService(RoundRepository roundRepository) {
		this.roundRepository = roundRepository;
	}
	
	public RoundSummaryResponse roundSummary() {
		
		ArrayList<Round> roundList = roundRepository.getAll();
		
		Long roundPlayed = roundList.stream().count();
		Long playerOneWinsTotal = roundList.stream().filter(r -> r.isPlayerOneWins()).count();
		Long playerTwoWinsTotal = roundList.stream().filter(r -> r.isPlayerTwoWins()).count();
		Long drawTotal = roundList.stream().filter(r -> r.isDraw()).count();
		
		return new RoundSummaryResponse(roundPlayed, playerOneWinsTotal, playerTwoWinsTotal, drawTotal);
	}
}

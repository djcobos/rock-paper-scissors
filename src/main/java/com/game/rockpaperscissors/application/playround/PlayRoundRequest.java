package com.game.rockpaperscissors.application.playround;

public class PlayRoundRequest {
	
	private String handsForPlayerOne;
	private String handsForPlayerTwo;

	public PlayRoundRequest(String handsToPlayerOne, String handsToPlayerTwo) {
		this.handsForPlayerOne = handsToPlayerOne;
		this.handsForPlayerTwo = handsToPlayerTwo;
	}

	public String getHandsForPlayerOne() {
		return handsForPlayerOne;
	}

	public String getHandsForPlayerTwo() {
		return handsForPlayerTwo;
	}
}

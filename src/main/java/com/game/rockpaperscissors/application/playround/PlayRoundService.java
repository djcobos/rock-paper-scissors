package com.game.rockpaperscissors.application.playround;

import java.util.List;

import org.springframework.stereotype.Service;

import com.game.rockpaperscissors.domain.player.Hands;
import com.game.rockpaperscissors.domain.player.Player;
import com.game.rockpaperscissors.domain.player.PlayerFactory;
import com.game.rockpaperscissors.domain.player.ResultStatus;
import com.game.rockpaperscissors.domain.round.ResultRoundService;
import com.game.rockpaperscissors.domain.round.Round;
import com.game.rockpaperscissors.domain.round.RoundRepository;


@Service
public class PlayRoundService {
	
	private ResultRoundService resultRoundService;
	private RoundRepository roundRepository;
	
	public PlayRoundService(ResultRoundService resultRoundService, RoundRepository roundRespository) {
		this.resultRoundService = resultRoundService;
		this.roundRepository = roundRespository;	
	}
	
	public PlayRoundResponse playRound(PlayRoundRequest playRoundRequest) throws Exception {
		
		List<String> listHandsPlayerOne = PlayerFactory.getListHandsByString(playRoundRequest.getHandsForPlayerOne());
		List<String> listHandsPlayerTwo = PlayerFactory.getListHandsByString(playRoundRequest.getHandsForPlayerTwo());
		
		Player playerOne = new Player(listHandsPlayerOne);
		Player playerTwo = new Player(listHandsPlayerTwo);
		
		ResultStatus resultStatus = resultRoundService.calculate(playerOne, playerTwo);
		Round round = Round.createRoundByResultStatus(resultStatus);
		roundRepository.save(round);
		
		return new PlayRoundResponse(
				Hands.getHandByValue(playerOne.getHandPlayed()), 
				Hands.getHandByValue(playerTwo.getHandPlayed()), 
				resultStatus.getName()
				);
	}
}


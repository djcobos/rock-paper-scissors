package com.game.rockpaperscissors.application.playround;

public class PlayRoundResponse {
	
	private String playerOneHand;
	private String playerTwoHand;
	private String roundResult;
	
	public PlayRoundResponse(String playerOneHand, String playerTwoHand, String roundResult) {
		this.playerOneHand = playerOneHand;
		this.playerTwoHand = playerTwoHand;
		this.roundResult = roundResult;
	}

	public String getPlayerOneHand() {
		return playerOneHand;
	}

	public String getPlayerTwoHand() {
		return playerTwoHand;
	}

	public String getRoundResult() {
		return roundResult;
	}
}


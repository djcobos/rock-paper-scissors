package com.game.rockpaperscissors.domain.round;

import com.game.rockpaperscissors.domain.player.Player;
import com.game.rockpaperscissors.domain.player.ResultStatus;

public class ResultRoundService {

	public ResultStatus calculate(Player player1, Player player2) {

		int modValue = (3 + player1.getHandPlayed() - player2.getHandPlayed()) % 3;
	
		if (isDraw(modValue)) {
			return ResultStatus.DRAW;
		} 
		
		if (isPar(modValue)) {
			return ResultStatus.PLAYER_TWO_WINS;
		} 
		
		return ResultStatus.PLAYER_ONE_WINS;
			
	}
	
	private boolean isDraw(int value) {
		return value == 0;
	}
	
	private boolean isPar(int value) {
		return value % 2 == 0;
	}
}

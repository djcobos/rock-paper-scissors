package com.game.rockpaperscissors.infrastructure.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.game.rockpaperscissors.application.roundsummary.RoundSummaryResponse;
import com.game.rockpaperscissors.application.roundsummary.RoundSummaryService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class RoundSummaryController {

	private final RoundSummaryService roundSummaryService;
	
	private ObjectMapper objectMapper;

	public RoundSummaryController(final RoundSummaryService roundSummaryService, ObjectMapper objectMapper) {
		this.roundSummaryService = roundSummaryService;
		this.objectMapper = objectMapper;
	}
	
	@RequestMapping(
			value = "/roundsummary", 
			method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> roundSummary() {
		
		RoundSummaryResponse roundSummaryResponse;
		String bodyResponse;
		
		try {
			roundSummaryResponse = roundSummaryService.roundSummary();
			bodyResponse = objectMapper.writeValueAsString(roundSummaryResponse);
		} catch (Exception ex) {
			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<String>(bodyResponse, HttpStatus.OK);
	}
}

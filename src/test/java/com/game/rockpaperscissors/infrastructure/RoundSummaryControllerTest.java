package com.game.rockpaperscissors.infrastructure;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.rockpaperscissors.application.roundsummary.RoundSummaryResponse;
import com.game.rockpaperscissors.application.roundsummary.RoundSummaryService;
import com.game.rockpaperscissors.infrastructure.controller.RoundSummaryController;



@RunWith(MockitoJUnitRunner.class)
public class RoundSummaryControllerTest {
	
	private RoundSummaryController roundSummaryController;
	private RoundSummaryService roundSummaryService;
	private MockMvc mockMvc;
	
	@Before
	public void setUp() throws Exception {
		roundSummaryService = Mockito.mock(RoundSummaryService.class);
		roundSummaryController = new RoundSummaryController(roundSummaryService, new ObjectMapper());
		this.mockMvc = MockMvcBuilders.standaloneSetup(roundSummaryController).build();
	}
	
	@Test
	public void testRoundSummaryIsOK() throws Exception {

		String controllerPath = "/roundsummary";
		RoundSummaryResponse roundSummaryResponse = new RoundSummaryResponse(6L, 3L, 2L, 1L );
		
		when(roundSummaryService.roundSummary()).thenReturn(roundSummaryResponse);

		mockMvc.perform(get(controllerPath)).andExpect(status().isOk());
		verify(roundSummaryService, times(1)).roundSummary();
	}
	
}
